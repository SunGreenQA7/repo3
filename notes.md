Дії та команди, які треба виконати у наступних ситуаціях:

1. Додати нове домашнє завдання в новий репозиторій на GitLab.
- авторизуємось на GitLab, натискаєм кнопку "+" у боковому меню, обираєм в розділі "In GitLab" команду "New project/repository"
- далі обираємо "Create blanc project"
- вводимо в поле Project name назву нашого репозиторію, у полі "Project deployment target" обираємо "No deployment planned",  перемикач  "Visibility Level" ставимо на "Public", знімаємо відмітку "Initialize repository with a README" у розділі "Project Configuration" і натискаємо кнопку "Create project"
- на новій сторінці натискаємо кнопку "Clone", із розділу "Clone with HTTPS" копіюємо адресу нашого репозиторію в буфер обміну
- переходимо в консоль і у каталозі із новим домашнім завданням виконуєм команди:
    git init
    git add .
    git commit -m "comment here your commit"
    
Цими командами ми створили новий локальний репозиторій і виконали знімок стану всіх об"єктів у каталозі домашнього завдання. Далі до локального репозиторію підключаємо наш новий пустиій репозиторій із GitLab:
    git remote add orig_repo1_hw2 https://gitlab.com/SunGreenQA7/repo1_hw2.git
Передаємо локальний репозиторій "orig_repo1_hw2" на віддалений GitLab master
    git push orig_repo1_hw2 master

2. У терміналі виходимо із поточної папки "cd .." та переходимо у папку другого репозиторію "cd repo2_hw3" 
    - виконуємо всі кроки як у п.1 із урахуванням імені папки repo2_hw3